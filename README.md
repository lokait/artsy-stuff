# Artsy Stuff

Collection of various artsy stuff I have made and shown publicly recently. For now only banners for ricing contest I made for the Linux Mint community on Discord.

Stay safe! :)

## License.
Copyleft work. Free Art License 1.3. See [LICENSE](/LICENSE) file or [artlibre.org](https://artlibre.org/licence/lal/en).
